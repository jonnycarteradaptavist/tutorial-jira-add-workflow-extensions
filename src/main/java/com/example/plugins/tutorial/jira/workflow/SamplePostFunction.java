package com.example.plugins.tutorial.jira.workflow;

import java.util.HashMap;
import java.util.Map;

import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.IssueInputParameters;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.workflow.function.issue.AbstractJiraFunctionProvider;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.WorkflowException;

import com.opensymphony.workflow.spi.WorkflowEntry;
import org.ofbiz.core.entity.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.bc.issue.IssueService.IssueResult;
import com.atlassian.jira.bc.issue.IssueService.TransitionValidationResult;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.IssueInputParameters;
import com.opensymphony.workflow.spi.WorkflowEntry;;
import org.ofbiz.core.entity.DelegatorInterface;
import org.ofbiz.core.entity.GenericValue;
import com.atlassian.jira.ofbiz.FieldMap;


/**
 * This is the post-function class that gets executed at the end of the transition.
 * Any parameters that were saved in your factory class will be available in the transientVars Map.
 */
public class SamplePostFunction extends AbstractJiraFunctionProvider {
    private static final Logger log = LoggerFactory.getLogger(SamplePostFunction.class);
    public static final String FIELD_MESSAGE = "messageField";

    public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
        MutableIssue issue = getIssue(transientVars);
        String message = (String) transientVars.get(FIELD_MESSAGE);

        DelegatorInterface gd = ComponentAccessor.getComponent(DelegatorInterface.class);
        Map<String, Long> stuff = new HashMap<String, Long>();
        stuff.put("id", issue.getWorkflowId());
        try {
            GenericValue gv = gd.findByPrimaryKey("OSWorkflowEntry", stuff);
            if (gv == null) {
                log.error("No OSWorkflowEntry - this will happen if the function is on the initial transition and placed before 'Creates the issue originally. Failed to do transition'");
                return;
            }
            if (gv.get("state").equals(WorkflowEntry.CREATED)) {
                gv.set("state", WorkflowEntry.ACTIVATED);
                try {
                    gv.store();
                } catch (GenericEntityException e) {
                    log.error("Nuts");
                }
            }

            IssueService issueService = ComponentAccessor.getIssueService();
            IssueInputParameters issueInputParameters = issueService.newIssueInputParameters();
            issueInputParameters.setAssigneeId("anuser"); //use arbitrary valid username here
            IssueService.TransitionValidationResult transitionValidationResult = issueService.validateTransition(issue.getCreator(), issue.getId(), 2, issueInputParameters);
            if (transitionValidationResult.isValid()) {
                IssueService.IssueResult transitionResult = issueService.transition(issue.getCreator(), transitionValidationResult);
                if (!transitionResult.isValid()) {
                    log.error("Transition failed - result is invalid");
                }
            } else {
                log.error("Transition not valid");
            }

        } catch (GenericEntityException e) {
            log.error("Nuts");
        }
    }
}